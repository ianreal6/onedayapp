package com.onedayapp.repo;

import java.util.List;

import com.onedayapp.model.Item;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface ItemRepo extends JpaRepository<Item, Long>{
  public Item findByItemId(long itemId);
}
