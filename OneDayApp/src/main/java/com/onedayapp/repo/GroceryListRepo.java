package com.onedayapp.repo;

import java.util.List;

import com.onedayapp.model.GroceryList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface GroceryListRepo extends JpaRepository<GroceryList, Long> {
  public GroceryList findByListId(long listId);
}
