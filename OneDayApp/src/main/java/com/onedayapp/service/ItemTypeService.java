package com.onedayapp.service;

import com.onedayapp.model.ItemType;
import com.onedayapp.repo.ItemTypeRepo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor = @_(@Autowired))
@NoArgsConstructor
public class ItemTypeService {
  private ItemTypeRepo itemTypeRepo;

  public void insertItemType(String category){
    ItemType newItemType = ItemType.builder()
      .category(category)
      .build();
    itemTypeRepo.save(newItemType);
  }
}
