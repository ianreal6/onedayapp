package com.onedayapp.service;

import com.onedayapp.model.Item;
import com.onedayapp.dto.ItemDto;
import com.onedayapp.repo.ItemRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor = @_(@Autowired))
@NoArgsConstructor
public class ItemService {
  public ItemRepo itemRepo;

  public void insertItem(ItemDto iDto){
    Item newItem = Item.builder()
      .itemName(iDto.getItemName())
      .category(iDto.getCategory())
      .itemCost(iDto.getItemCost())
      .build();
    itemRepo.save(newItem);
  }
}
