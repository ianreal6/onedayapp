package com.onedayapp.service;

import com.onedayapp.model.Item;
import com.onedayapp.model.GroceryList;
import com.onedayapp.repo.ItemRepo;
import com.onedayapp.repo.GroceryListRepo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor = @_(@Autowired))
@NoArgsConstructor
public class GroceryListService {
  public GroceryListRepo glRepo;
  public ItemRepo iRepo;
  public List<GroceryList> getAllLists(){
    return glRepo.findAll();
  }

  public GroceryList getListById(Long listId){
    return glRepo.findByListId(listId);
  }

  public void addToList(Long lId, Long iId){
    GroceryList gList = getListById(lId);
    Item item = iRepo.findByItemId(iId);
    gList.add(item);
    glRepo.save(gList);
  }

  public void removeFromList(Long lId, Long iId){
    GroceryList gList = getListById(lId);
    Item item = iRepo.findByItemId(iId);
    gList.remove(item);
    glRepo.save(gList);
  }

}
