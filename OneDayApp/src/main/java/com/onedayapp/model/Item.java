package com.onedayapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.onedayapp.model.ItemType;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Item {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(updatable = false)
  private Long itemId;

  @Column(unique = true, nullable = false)
  private String itemName;

  @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JsonIgnore
  private ItemType category;

  @Column(nullable = false)
  private double itemCost;

  @ManyToMany
  @JoinColumn(name = "grocery_list_items")
  @JsonIgnore
  private List<GroceryList> groceryLists;
  

  

}
