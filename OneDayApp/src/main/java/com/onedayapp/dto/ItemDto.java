package com.onedayapp.dto;

import com.onedayapp.model.ItemType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Builder;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemDto {
  private String itemName;

  private ItemType category;

  private double itemCost;
}
