package com.onedayapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneDayAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneDayAppApplication.class, args);
	}

}
