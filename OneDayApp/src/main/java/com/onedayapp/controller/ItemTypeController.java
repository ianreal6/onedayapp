package com.onedayapp.controller;


import com.onedayapp.service.ItemTypeService;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@RestController
@RequestMapping(value="/itemtype")
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class ItemTypeController {
  private ItemTypeService itServ;

  @PostMapping("/add/{category}")
  public ResponseEntity<String> addItem(@PathVariable String category){
    itServ.insertItemType(category);
    return new ResponseEntity<>("Item Type added to Grand List", HttpStatus.CREATED);
  }
}
