package com.onedayapp.controller;

import java.util.List;

import com.onedayapp.model.GroceryList;
import com.onedayapp.service.ItemService;
import com.onedayapp.service.GroceryListService;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@RestController
@RequestMapping(value="/grocerylist")
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class GroceryListController {
  private GroceryListService gcServ;

  @GetMapping("/all")
  public ResponseEntity<List<GroceryList>> getall(){
    return new ResponseEntity<>(gcServ.getAllLists(), HttpStatus.CREATED);
  }

  @GetMapping("/{lId}")
  public ResponseEntity<GroceryList> getById(@PathVariable("lId") Long lId){
    return new ResponseEntity<>(gcServ.getListById(lId), HttpStatus.ACCEPTED);
  }

  @GetMapping("/add/{lId}/add/{iId}")
  public ResponseEntity<String> addById(@PathVariable("lId") Long lId, @PathVariable("iId") Long iId){
    gcServ.addToList(lId, iId);
    return new ResponseEntity<>("Item added to list",HttpStatus.ACCEPTED);
  }

  @GetMapping("/remove/{lId}/add/{iId}")
  public ResponseEntity<String> removeById(@PathVariable("lId") Long lId, @PathVariable("iId") Long iId){
    gcServ.removeFromList(lId, iId);
    return new ResponseEntity<>("Item added to list",HttpStatus.ACCEPTED);
  }
}
