package com.onedayapp.controller;


import java.util.List;

import com.onedayapp.service.ItemService;
import com.onedayapp.dto.ItemDto;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@RestController
@RequestMapping(value="/item")
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class ItemController {
  private ItemService iServ;

  @PostMapping("/add")
  public ResponseEntity<String> addItem(@RequestBody ItemDto iDto){
    iServ.insertItem(iDto);
    return new ResponseEntity<>("Item added to Grand List", HttpStatus.CREATED);
  }
  
}
