package com.onedayapp.repo;

import java.util.List;

import com.onedayapp.model.ItemType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface ItemTypeRepo extends JpaRepository<ItemType, Long>{
  
}
